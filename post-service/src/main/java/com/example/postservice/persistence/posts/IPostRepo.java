package com.example.postservice.persistence.posts;

import com.example.postservice.model.posts.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IPostRepo extends MongoRepository<Post, UUID> {
     List<Post> getAllByAuthorID(UUID authorID);
     void deleteAllByAuthorID(UUID authorID);


//     List<Post> findAllByAuthorIDAndIsInactiveFalse(UUID authorID);

     //Set post inactive by authorID

//     void findByPostIDAndIsInactive();

//     List<Post> findAllByContentContainingOrTopicContaining(String topic);
     void deleteByPostIDAndAuthorID(UUID authorID,UUID postID);

     Post getByPostID(UUID postId);

     void deleteByPostID(UUID postID);

//     void updateByPostID(UUID postID);
//     Post findPostByPostID(UUID postId);
}
