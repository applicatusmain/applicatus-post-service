package com.example.postservice.controller.posts.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdatePostDetailsResponse {
    private String title;
    private String content;
    private LocalDateTime postedAt;
//    private String userName;
}
