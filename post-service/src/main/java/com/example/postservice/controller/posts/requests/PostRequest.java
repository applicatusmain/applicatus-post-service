package com.example.postservice.controller.posts.requests;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.util.UUID;

public class PostRequest {

    private UUID postID;
    private UUID postAuthor;
    private String topic;
    private String content;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime postedAt;
    private int likeCount;
    private Boolean isInactive;
}
