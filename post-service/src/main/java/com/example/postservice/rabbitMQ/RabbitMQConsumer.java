package com.example.postservice.rabbitMQ;

import com.example.postservice.model.posts.Post;
import com.example.postservice.service.posts.PostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RabbitMQConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQConsumer.class);

    @Autowired
    private PostService postService;


    @RabbitListener(queues = "post_json")
    public void receivePost(Post post){
        LOGGER.info(String.format("Received message... ->%s", post.toString()));
        postService.createPost(postService.mapToPostRequest(post));
    }
}
