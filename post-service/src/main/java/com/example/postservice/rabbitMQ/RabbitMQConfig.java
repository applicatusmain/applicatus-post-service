package com.example.postservice.rabbitMQ;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//import java.util.Queue;

@Configuration

public class RabbitMQConfig {

    @Bean
    public Queue postQueue() { return new Queue("post");}

    @Bean Queue postJsonQueue() { return new Queue("post_json");}
    @Bean
    public TopicExchange postExchange() {return new TopicExchange("post_exchange");}

    @Bean
    public Binding postJsonBinding(){
        return BindingBuilder
                .bind(postJsonQueue())
                .to(postExchange())
                .with("post_routing_key");
    }

    @Bean
    public MessageConverter converter() { return  new Jackson2JsonMessageConverter();}



    @Bean
    public AmqpTemplate amqpTemplate(ConnectionFactory connectionFactory){
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }
}


