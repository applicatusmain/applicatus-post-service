package com.example.postservice.service.filter;

import java.util.List;

public interface SearchStrategy<T> {
    List<T> search(List<T> list, String keyword);
}
