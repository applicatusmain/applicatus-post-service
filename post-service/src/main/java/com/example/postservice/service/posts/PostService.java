package com.example.postservice.service.posts;

import com.example.postservice.controller.posts.requests.CreatePostRequest;
import com.example.postservice.controller.posts.requests.UpdatePostDetailsRequest;
import com.example.postservice.controller.posts.response.CreatePostResponse;
import com.example.postservice.controller.posts.response.GetPostResponse;
import com.example.postservice.controller.posts.response.UpdatePostDetailsResponse;
import com.example.postservice.exceptionHandler.DeleteRequestFailedException;
import com.example.postservice.model.posts.Post;
import com.example.postservice.persistence.posts.IPostRepo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
//import org.springframework.web

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class PostService implements IPostService {

    private final IPostRepo postRepository;
//    public WebClient webClient;
//    private final Auth authorizationService;



//DataTranfer Prep

    public UpdatePostDetailsResponse mapToUpdatePostDetailsResponse(Post post){
        return UpdatePostDetailsResponse.builder()
                .title(post.getTopic())
                .content(post.getContent())
                .postedAt(LocalDateTime.now()).build();
    }

    public UpdatePostDetailsRequest mapToUpdatePostDetailsRequest(Post post){
        return UpdatePostDetailsRequest.builder()
                .authorID(post.getAuthorID())
                .topic(post.getTopic())
                .content(post.getContent())
                .postedAt(LocalDateTime.now()).build();
    }

    public CreatePostRequest mapToPostRequest(Post request){
        return CreatePostRequest.builder()
                .postID(UUID.randomUUID())
                .postAuthor(request.getAuthorID())
                .topic(request.getTopic())
                .content(request.getContent())
                .likeCount(request.getLikeCount())
                .postedAt(request.getPostedAt())
                .isInactive(request.getIsDeleted())
                .build();
    }
    public CreatePostResponse createPost(CreatePostRequest request){
        Post newPost = savePost(request);
        return CreatePostResponse.builder()
                .postID(UUID.randomUUID())
                .postAuthor(newPost.getAuthorID())
                .topic(newPost.getTopic())
                .content(newPost.getContent())
                .likeCount(newPost.getLikeCount())
                .postedAt(newPost.getPostedAt())
                .isInactive(newPost.getIsDeleted())
                .build();
    }

//    public PostRabbitMQ mapToPostRabbitMQ

    public CreatePostResponse mapToPostResponse(Post post){
        return CreatePostResponse.builder()
                .postID(post.getPostID())
                .postAuthor(post.getAuthorID())
                .topic(post.getTopic())
                .content(post.getContent())
                .likeCount(post.getLikeCount())
                .postedAt(post.getPostedAt())
                .isInactive(post.getIsDeleted())
                .build();

    }

    public UpdatePostDetailsResponse mapToPostUpdateResponse(Post post){
        return         UpdatePostDetailsResponse.builder()
                .content(post.getContent())
                .title(post.getTopic())
                .postedAt(post.getPostedAt())
                .build();
    }

    @Override
    public Post savePost(CreatePostRequest request) {
        Post newPost = Post.builder()
                .postID(UUID.randomUUID())
                .authorID(request.getPostAuthor())
                .topic(request.getTopic())
                .content(request.getContent())
                .likeCount(request.getLikeCount())
                .postedAt(request.getPostedAt())
                .isDeleted(request.getIsInactive())
                .build();
        return postRepository.save(newPost);

    }

    // CRUD


    //    Create ===================================



    //    Read ===================================

//    @Override
//    public Post findPostById(UUID postId) {
//        return null;
//    }

    @Override
    public List<CreatePostResponse> getAllPosts() {

        List<Post> allPosts = postRepository.findAll().stream().collect(Collectors.toList());
        List<CreatePostResponse> postResponses = allPosts.stream().map(this::mapToPostResponse).toList();
        return postResponses;
    }
//    public ResponseEntity deactivatePostsWithAuthorID(UUID authorID) {
//        {
////            return ResponseEntity.ok();
//        }

    public ResponseEntity deactivatePostWithPostID(UUID postID) {
//        postRepository.deleteAllByAuthorID(authorID);
        Post post = postRepository.getByPostID(postID);
//        for (Post post: postList) {
            post.setIsDeleted(true);
            
//        }
        postRepository.save(post);
        return ResponseEntity.ok("PostAuthor went well");
    }
    public ResponseEntity deactivateAllPostsWithAuthorID(UUID authorID) {
//        postRepository.deleteAllByAuthorID(authorID);
        List<Post> postList = postRepository.getAllByAuthorID(authorID);
        for (Post post: postList) {
            post.setIsDeleted(true);
        }
        postRepository.saveAll(postList);
        return ResponseEntity.ok("PostAuthor went well");
    }

    public ResponseEntity deletePostWithPostID(UUID postID){
        postRepository.deleteByPostID(postID);
        return ResponseEntity.ok("Post has been deleted");
    }
    public List<Post> getAllPostWithAuthorID(UUID authorID){
        List<Post> authorPostID = postRepository.getAllByAuthorID(authorID);
        if(!authorPostID.isEmpty()){
            for (Post post:authorPostID.stream().toList()
                 ) {
                if (post.getAuthorID()==authorID){
//                    postRepository.deleteByPostIDAndAuthorID(post.getPostID(),post.getAuthorID());
                    //Log this has been deleted
                }
                else {
                    continue;
                }


            }
            return authorPostID;
        }
        return null;
    }

    //Update
    @Override
    public UpdatePostDetailsResponse updatePost(UpdatePostDetailsRequest request) {
//        String result = webClient.get("http://localhost:8087")
//       Post id need to be correct to not get 405
        Post post = postRepository.getByPostID(request.getPostID());
        post.setContent(request.getContent());
        post.setTopic(request.getTopic());
        post.setPostedAt(LocalDateTime.now());
        post.setPostID(request.getPostID());


//        Put a log that its post set and set to save
        postRepository.save(post);


        return mapToPostUpdateResponse(post);
    }

    //Delete ===================================
    //Need Admin privelages or the same user to do this action
    public ResponseEntity deleteAllPostsWithAuthorID(UUID authorID){
//        postRepository.deleteAllByAuthorID(authorID);

        List<Post> authorPostID = postRepository.getAllByAuthorID(authorID);
        if(!authorPostID.isEmpty()){
//            log.info(authorPostID.toString());
            for (Post post:authorPostID.stream().toList()
            ) {
                log.info(post.toString());
                log.info(post.getAuthorID().toString());
                log.info(authorID.toString());
                if (post.getAuthorID().equals(authorID)){
                    postRepository.deleteByPostID(post.getPostID());
                    //Log this has been deleted
                }




            }
            return ResponseEntity.ok("Post's have been deleted");
        }
//        return ResponseEntity.ok("Post's have been deleted");


        return ResponseEntity.ofNullable("No posts have been deleted");
    }

    @Override
    public void deletePost(UUID postID) {
        Post post = postRepository.getByPostID(postID);
        if(post != null) {

            post.setIsDeleted(true);
            postRepository.save(post);
        }
        else {
            throw new DeleteRequestFailedException();
        }
    }






    public Post findPostWithPostID(UUID postID) {
       Post post = postRepository.getByPostID(postID);
        if(post != null){
            return post;
        }
        return null;
    }

//    @Override
//    public Optional<Post> findPostInformation(String username) {
//        return Optional.empty();
//    }




}
