package com.example.postservice.service.posts;

import com.example.postservice.controller.posts.requests.CreatePostRequest;
import com.example.postservice.controller.posts.requests.UpdatePostDetailsRequest;
import com.example.postservice.controller.posts.response.CreatePostResponse;
import com.example.postservice.controller.posts.response.GetPostResponse;
import com.example.postservice.controller.posts.response.UpdatePostDetailsResponse;
import com.example.postservice.model.posts.Post;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IPostService {

//    List<Post> getAllPosts();
// Crud
    List<CreatePostResponse> getAllPosts();
    CreatePostResponse createPost(CreatePostRequest request);

    Post savePost(CreatePostRequest request);

    UpdatePostDetailsResponse updatePost (UpdatePostDetailsRequest request);

    void deletePost(UUID postID);

//    Post findPostById(UUID postId);

//    CreatePostRequest CreatePost(CreatePostRequest request);

//    List<CreatePostResponse> findAll();

//    List<Post> findAllByUserID(UUID userId);

//    List<CreatePostResponse> searchPosts(String input);


//
//    Optional<Post> findPostInformation(String username);
}
